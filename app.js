
//Budget Controller
var budgetController = (function () {
    
    var Expense = function (id, description, value) {
        this.id = id,
        this.description = description,
        this.value = value,
        this.percentage = -1;
    };

    Expense.prototype.calcPercentage = function (totalIncome) {

        if (totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);             
        } else {
            this.percentage = -1;
        }
    };

    Expense.prototype.getPercentage = function () {
        return this.percentage;
    };

    var Income = function (id, description, value) {
        this.id = id,
        this.description = description,
        this.value = value;
    };

    var calcTotal = function (type) {
        var sum = 0;

        data.allItems[type].forEach(function(cur) {
            sum += cur.value;
        });

        data.totals[type] = sum;
    };

    var data = {
        allItems: {
            exp:[],
            inc:[]
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };

    return {

        addItem: function (type, desc, val)  {
            var newItem;

            //create new id
            if (data.allItems[type].length > 0) {
                id = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                id = 0;
            }
            
            //create new item based on inc or exp type
            if (type === 'exp') {
                newItem = new Expense(id, desc, val);
            } else if (type === 'inc') {
                newItem = new Income(id, desc, val);
            };

            //push into data structure
            data.allItems[type].push(newItem);

            //return new element
            return newItem;
            
        },

        deleteItem: function (type, id) {
            var ids, index;
            
            ids = data.allItems[type].map(function (curr) {
                    return curr.id; 
                  });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }

        },

        calculateBudget: function () {
            //calculate total income and expenses
            calcTotal('exp');
            calcTotal('inc');
            //calculate the budget: income minus expenses
            data.budget = data.totals.inc - data.totals.exp;

            //calculate the percentage of income spent
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }       
        },

        calculatePercentages: function () {            
            data.allItems.exp.forEach(function (cur) {
                cur.calcPercentage(data.totals.inc); 
            });
        },

        getPercentages: function () {
            var allPercentages = data.allItems.exp.map(function (cur) {
                return cur.getPercentage();
            });

            return allPercentages;
        },

        getBudget: function () {
            return{
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }  
        },

        testing: function () {
            console.log(data);
        }
    }

})();


//UI Controller
var UiController = (function () {
    
    var domStrings = {
        inputType: '.add__type',
        inputDesc: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer:'.income__list',
        expenseContainer:'.expenses__list',
        budgetLabel:'.budget__value',
        incomeLabel: '.budget__income--value',
        expenseLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLabel:'.budget__title--month'
    };

    var formatNumber = function (num, type) {
        var numSplit, int, dec;
        
        num = Math.abs(num);
        num = num.toFixed(2);
        numSplit = num.split('.');

        int = numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        dec = numSplit[1];            
        
        return (type === 'exp' ?  '-' : '+') + ' ' + int +'.'+ dec;
    };

    var nodeListForEach = function (list, callBack) {
        for (var i = 0; i < list.length; i++) {
            callBack(list[i], i);
            
        }
    };

    return{

        getInput: function () {
            return{
                type: document.querySelector(domStrings.inputType).value, // either "inc" or "exp"
                desc: document.querySelector(domStrings.inputDesc).value,
                value: parseFloat(document.querySelector(domStrings.inputValue).value)
            };
        },

        addListItem: function(obj, type){
            var html, newHtml, element;

            //create html string with placeholder text
            if (type === 'inc') {
                
                element = domStrings.incomeContainer;

                html = `<div class="item clearfix" id="income-%id%">
                            <div class="item__description">%description%</div>
                                <div class="right clearfix">
                                    <div class="item__value">%value%</div>
                                    <div class="item__delete">
                                        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>
                                    </div>
                                </div>
                        </div>`;

            } else if (type === 'exp') {

                element = domStrings.expenseContainer;

                html = `<div class="item clearfix" id="expense-%id%">
                            <div class="item__description">%description%</div>
                                <div class="right clearfix">
                                    <div class="item__value">%value%</div>
                                    <div class="item__percentage">21%</div>
                                    <div class="item__delete">
                                        <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>
                                    </div>
                                </div>
                        </div>`;
            }                         

            //replace placeholder with actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type));

            //add the html to the dom
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);

        },

        deleteListItem: function (selectorID) {
            var element = document.getElementById(selectorID)

            element.parentNode.removeChild(element);
        },

        clearFields: function () {
            var fields, fieldsArr;

            fields = document.querySelectorAll(domStrings.inputDesc + ', ' + domStrings.inputValue);

            fieldsArr =  Array.prototype.slice.call(fields);  

            fieldsArr.forEach(function(curr, index, array) {
                curr.value = "";
            });

            fieldsArr[0].focus();
        },

        displayBudget: function (obj) {
            var type;
            obj.budget > 0 ? type ='inc' : type = 'exp';

            document.querySelector(domStrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(domStrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc');
            document.querySelector(domStrings.expenseLabel).textContent = formatNumber(obj.totalExp, 'exp');
            

            if (obj.percentage > 0) {
                document.querySelector(domStrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(domStrings.percentageLabel).textContent = '---';
            }
        },

        displayPercentages: function (percentages) {            
            var fields = document.querySelectorAll(domStrings.expensesPercLabel);            

            nodeListForEach(fields, function (current, index) {
                if (percentages[index] > 0) {
                    current.textContent = percentages[index] + '%';
                } else {
                    current.textContent = '---';
                }
            });
        },        

        displayMonth: function () {
            var now, year, month, months;

            now = new Date();
            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            month = now.getMonth();
            year = now.getFullYear();
            document.querySelector(domStrings.dateLabel).textContent = months[month] + ' ' + year
        },

        changedType: function () {
            var fields = document.querySelectorAll(
                domStrings.inputType +','+
                domStrings.inputDesc +','+
                domStrings.inputValue
            );

            nodeListForEach(fields, function (cur) {
                 cur.classList.toggle('red-focus');
            });

            document.querySelector(domStrings.inputBtn).classList.toggle('red');
        },

        getDomStrings: function () {
            return domStrings;
        }
    };

})();

//Global App Controller
var controller = (function (budgetCtrl, UiCtrl) {
    
    var setUpEventListeners = function (params) {
        var DOM = UiCtrl.getDomStrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);

        document.querySelector(DOM.inputType).addEventListener('change', UiCtrl.changedType);
    };  
    
    var updateBudget = function () {
        //1. Calculate the budget
        budgetCtrl.calculateBudget();

        //2. Return the budget
        var budget = budgetCtrl.getBudget();

        //3. Display the budget on the UI
        UiCtrl.displayBudget(budget);
    };

    var updatePercentages = function () {
        //1.calc percentages
        budgetCtrl.calculatePercentages();

        //2. read percentages from the budget controller
        var percentages = budgetCtrl.getPercentages();

        //3. Update the UI with the new percentages
        UiCtrl.displayPercentages(percentages);
    };

    var ctrlAddItem = function () {
        var input, newItem;

        //1. Get input data
        input = UiCtrl.getInput();

        if (input.desc !== '' && !isNaN(input.value) && input.value > 0) {
            //2. Add item to budget controller
            newItem = budgetCtrl.addItem(input.type, input.desc, input.value);

            //3. add to the UI
            UiCtrl.addListItem(newItem, input.type);

            //4. Clear the input fields
            UiController.clearFields();

            //5. Calculate and update budget
            updateBudget();
            
            updatePercentages();
        };        

    };

    var ctrlDeleteItem = function (event) {
        var itemId, splitId, type, id;
        
        itemId = event.target.parentNode.parentNode.parentNode.parentNode.id;
        
        if (itemId) {
            splitId = itemId.split('-');

            if (splitId[0] === 'income') {
                type = 'inc';
            }else  {
                type = 'exp';
            }

            id = parseInt(splitId[1]);

            //remove the item from the budget array
            budgetCtrl.deleteItem(type, id);

            //remove item from the ui
            UiCtrl.deleteListItem(itemId);

            //update the budget 
            updateBudget();

            updatePercentages();
        }

    };

    return{
        init: function () {
            UiCtrl.displayMonth();
             UiCtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: -1
            });
            setUpEventListeners();
        }
    };
    
})(budgetController, UiController);

controller.init();


